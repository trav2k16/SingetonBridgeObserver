using System;
using System.Collections.Generic;

public interface IHeartRateSensor
{
    int GetHeartRate();
    void StartMonitoring();
    void StopMonitoring();
    void AddMonitor(IMonitor monitor);
    void RemoveMonitor(IMonitor monitor);
    void RegisterObserver(IObserver observer);
    void UnregisterObserver(IObserver observer);
}

public class HeartRateSensor : IHeartRateSensor
{
    private static HeartRateSensor instance;
    private List<IMonitor> monitors;
    private List<IObserver> observers;

    private HeartRateSensor()
    {
        monitors = new List<IMonitor>();
        observers = new List<IObserver>();
    }

    public static HeartRateSensor GetInstance()
    {
        if (instance == null)
        {
            instance = new HeartRateSensor();
        }
        return instance;
    }

    public int GetHeartRate()
    {
        Random random = new Random();
        return random.Next(60, 100);
    }

    public void StartMonitoring()
    {
        int heartRate = GetHeartRate();
        foreach (IMonitor monitor in monitors)
        {
            monitor.DisplayHeartRate(heartRate);
        }
        NotifyObservers(heartRate);
    }

    public void StopMonitoring()
    {
        // Здесь должна быть реализация остановки мониторинга сердечного ритма
    }

    public void AddMonitor(IMonitor monitor)
    {
        monitors.Add(monitor);
    }

    public void RemoveMonitor(IMonitor monitor)
    {
        monitors.Remove(monitor);
    }

    public void RegisterObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    public void UnregisterObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    private void NotifyObservers(int heartRate)
    {
        foreach (IObserver observer in observers)
        {
            observer.Update(heartRate);
        }
    }
}

public interface IMonitor
{
    void DisplayHeartRate(int heartRate);
}

public class SmartphoneMonitor : IMonitor
{
    public void DisplayHeartRate(int heartRate)
    {
        Console.WriteLine("Smartphone Monitor: Heart Rate - " + heartRate);
    }
}

public class ComputerMonitor : IMonitor
{
    public void DisplayHeartRate(int heartRate)
    {
        Console.WriteLine("Computer Monitor: Heart Rate - " + heartRate);
    }
}

public interface IObserver
{
    void Update(int heartRate);
}

public class User : IObserver
{
    public void Update(int heartRate)
    {
        Console.WriteLine("User: Heart Rate updated - " + heartRate);
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        IHeartRateSensor sensor = HeartRateSensor.GetInstance();

        IMonitor smartphoneMonitor = new SmartphoneMonitor();
        IMonitor computerMonitor = new ComputerMonitor();
        IObserver user = new User();

        sensor.AddMonitor(smartphoneMonitor);
        sensor.AddMonitor(computerMonitor);
        sensor.RegisterObserver(user);

        sensor.StartMonitoring();

        sensor.RemoveMonitor(smartphoneMonitor);
        sensor.StopMonitoring();

        sensor.UnregisterObserver(user);

        Console.ReadLine();
    }
}
