# Описание
В данной программе используются следующие паттерны для проектирования:
1. Одиночка - порождающий паттерн, который гарантирует, что у класса будет только один экземпляр в системе, и предоставляет глобальную точку доступа к этому экземпляру.
1. Мост - структурный паттерн, который разделяет абстракцию и реализацию таким образом, чтобы они могли изменяться независимо друг от друга. Это позволяет легко добавлять новые реализации без изменения кода абстракции.
1. Наблюдатель - поведенческий паттерн, который определяет отношение "один-ко-многим" между объектами, таким образом, что при изменении состояния одного объекта все зависимые от него объекты автоматически уведомляются и обновляются. Это позволяет создавать слабосвязанные системы, где объекты могут взаимодействовать без явных ссылок друг на друга.
## Описание приложения
У нас есть приложение для мониторинга здоровья пациента, которое собирает данные о его сердечном ритме. Здесь используется паттерн "Одиночка" для реализации класса "Сердечный датчик", который будет отвечать за сбор данных о сердечном ритме. Поскольку доступ к датчику должен быть глобальным, но объект этого класса должен быть единственным, создается класс "Сердечный датчик" с приватным конструктором и статическим методом getInstance(), который будет возвращать единственный экземпляр класса.

Далее, для связи с приложением используется паттерн "Мост". Создается абстрактный класс "Монитор", который представляет интерфейс для отображения данных сердечного датчика на разных устройствах. Затем создаются конкретные классы "Монитор на смартфоне" и "Монитор на компьютере", которые наследуются от абстрактного класса "Монитор" и реализуют его методы. Класс "Сердечный датчик" содержит ссылку на абстрактный класс "Монитор", и через эту ссылку данные о сердечном ритме будут передаваться и отображаться на разных устройствах.

Кроме того, используется паттерн "Наблюдатель" для оповещения пользователей о изменениях в сердечном ритме. Создается интерфейс "Наблюдатель", который определяет метод update(), и класс "Пользователь", который реализует этот интерфейс. Класс "Сердечный датчик" содержит список зарегистрированных наблюдателей, и при изменении данных о сердечном ритме он оповещает всех наблюдателей, вызывая их метод update().

Таким образом, приложение использует паттерн "Одиночка" для гарантии единственности экземпляра класса "Сердечный датчик", паттерн "Мост" для связи с различными устройствами и отображения данных на них, а также паттерн "Наблюдатель" для оповещения пользователей о изменениях в сердечном ритме.
# Диаграмма классов
```plantuml
@startuml
interface IHeartRateSensor {
  + GetHeartRate(): int
  + StartMonitoring(): void
  + StopMonitoring(): void
  + AddMonitor(monitor: IMonitor): void
  + RemoveMonitor(monitor: IMonitor): void
  + RegisterObserver(observer: IObserver): void
  + UnregisterObserver(observer: IObserver): void
}

class HeartRateSensor {
  - instance: HeartRateSensor
  - monitors: List<IMonitor>
  - observers: List<IObserver>
  + GetInstance(): HeartRateSensor
  + GetHeartRate(): int
  + StartMonitoring(): void
  + StopMonitoring(): void
  + AddMonitor(monitor: IMonitor): void
  + RemoveMonitor(monitor: IMonitor): void
  + RegisterObserver(observer: IObserver): void
  + UnregisterObserver(observer: IObserver): void
}

interface IMonitor {
  + DisplayHeartRate(heartRate: int): void
}

class SmartphoneMonitor {
  + DisplayHeartRate(heartRate: int): void
}

class ComputerMonitor {
  + DisplayHeartRate(heartRate: int): void
}

interface IObserver {
  + Update(heartRate: int): void
}

class User {
  + Update(heartRate: int): void
}

IHeartRateSensor <|.. HeartRateSensor
IMonitor <|.. SmartphoneMonitor
IMonitor <|.. ComputerMonitor
IObserver <|.. User

HeartRateSensor "1" --> "*" IMonitor
HeartRateSensor "1" --> "*" IObserver
@enduml


